netcdf case51he {

// global attributes:
		:SMS++_file_type = 1 ;
		:baseMVA = 1. ;

group: Block_0 {
  dimensions:
  	NumberElectricalGenerators = 1 ;
  	NumberNodes = 51 ;
  	NumberLines = 50 ;
  	TimeHorizon = 1 ;
  	NumberUnits = 1 ;
  variables:
  	string NetworkBlockClassname ;
  	uint GeneratorNode(NumberElectricalGenerators) ;
  	double ActivePowerDemand(NumberNodes, TimeHorizon) ;
  	double NodeConductance(NumberNodes) ;
  	double NodeSusceptance(NumberNodes) ;
  	double NodeMaxVoltage(NumberNodes) ;
  	double NodeMinVoltage(NumberNodes) ;
  	uint StartLine(NumberLines) ;
  	uint EndLine(NumberLines) ;
  	double LineResistance(NumberLines) ;
  	double LineReactance(NumberLines) ;
  	double LineSusceptance(NumberLines) ;
  	double LineRATEA(NumberLines) ;
  	double LineRatio(NumberLines) ;
  	double LineShiftAngle(NumberLines) ;
  	double LineMinAngle(NumberLines) ;
  	double LineMaxAngle(NumberLines) ;
  	double MinPowerFlow(NumberLines) ;
  	double MaxPowerFlow(NumberLines) ;

  // group attributes:
  		:type = "UCBlock" ;
  		:id = "0" ;
  data:

   NetworkBlockClassname = "ACNetworkBlock" ;

   GeneratorNode = 0 ;

   ActivePowerDemand =
  0,
  0.01458,
  0,
  0,
  0,
  0.01458,
  0,
  0,
  0.05833,
  0,
  0,
  0.02,
  0,
  0,
  0,
  0.02917,
  0.07292,
  0.02,
  0.14567,
  0.06288,
  0.01458,
  0.09479,
  0.01458,
  0,
  0.01458,
  0.035,
  0.02917,
  0.02917,
  0.09188,
  0.0859,
  0.02917,
  0.01458,
  0.04375,
  0.04375,
  0,
  0.14583,
  0.09188,
  0.02917,
  0.05833,
  0.01458,
  0.02917,
  0.02917,
  0.02917,
  0.14875,
  0.02917,
  0,
  0.02917,
  0.05833,
  0.04375,
  0.11667,
  0.09188 ;

   NodeConductance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0 ;

   NodeSusceptance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0 ;

   NodeMaxVoltage = 1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 
      1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 
      1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 
      1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1 ;

   NodeMinVoltage = 1, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 
      0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 
      0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 
      0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ;

   StartLine = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 
      2, 3, 4, 21, 22, 23, 24, 25, 22, 23, 6, 29, 30, 31, 32, 33, 34, 35, 36, 
      29, 34, 7, 8, 9, 10, 11, 13, 45, 46, 45, 14, 16 ;

   EndLine = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 
      19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 
      37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 ;

   LineResistance = 0.4214, 0.4214, 0.2107, 0.4214, 0.2107, 0.2107, 0.4214, 
      0.4214, 0.3996, 0.5328, 0.2664, 0.7992, 0.5328, 1.66675, 2.0001, 
      0.6667, 1.3334, 0.6667, 5.3336, 1.3334, 3.3335, 2.6668, 0.6667, 6.667, 
      1.3334, 2.0001, 5.3336, 1.3334, 5.00025, 0.6667, 1.3334, 1.3334, 
      1.00005, 1.3334, 2.33345, 1.3334, 1.00005, 2.0001, 1.3334, 1.00005, 
      1.3334, 4.0002, 1.3334, 4.6669, 0.6667, 2.0001, 2.0001, 0.13334, 
      4.6669, 2.6668 ;

   LineReactance = 0.7334, 0.7334, 0.3667, 0.7334, 0.3667, 0.3667, 0.7334, 
      0.7334, 0.67215, 0.8962, 0.4481, 1.3443, 0.8962, 1.102, 1.3224, 0.4408, 
      0.8816, 0.4408, 3.5264, 0.8816, 2.204, 1.7632, 0.4408, 4.408, 0.8816, 
      1.3224, 3.5264, 0.8816, 3.306, 0.4408, 0.8816, 0.8816, 0.6612, 0.8816, 
      1.5428, 0.8816, 0.6612, 1.3224, 0.8816, 0.6612, 0.8816, 2.6448, 0.8816, 
      3.0856, 0.4408, 1.3224, 1.3224, 0.08816, 3.0856, 1.7632 ;

   LineSusceptance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0 ;

   LineRATEA = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0 ;

   LineRatio = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0 ;

   LineShiftAngle = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0 ;

   LineMinAngle = -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360 ;

   LineMaxAngle = 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360 ;

   MinPowerFlow = -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000 ;

   MaxPowerFlow = 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000 ;

  group: UnitBlock_0 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	double StartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 0 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 10 ;

     MinReactivePower = -10 ;

     VoltageMagnitude = 1 ;

     MaxPower = 10 ;

     MinPower = 0 ;

     CostModel = 1 ;

     StartupCost = 0 ;

     PowerCostCoeffs = 0, 20, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_0
  } // group Block_0
}
