netcdf case4_dist {

// global attributes:
		:SMS++_file_type = 1 ;
		:baseMVA = 1. ;

group: Block_0 {
  dimensions:
  	NumberElectricalGenerators = 2 ;
  	NumberNodes = 4 ;
  	NumberLines = 3 ;
  	TimeHorizon = 1 ;
  	NumberUnits = 2 ;
  variables:
  	string NetworkBlockClassname ;
  	uint GeneratorNode(NumberElectricalGenerators) ;
  	double ActivePowerDemand(NumberNodes, TimeHorizon) ;
  	double NodeConductance(NumberNodes) ;
  	double NodeSusceptance(NumberNodes) ;
  	double NodeMaxVoltage(NumberNodes) ;
  	double NodeMinVoltage(NumberNodes) ;
  	uint StartLine(NumberLines) ;
  	uint EndLine(NumberLines) ;
  	double LineResistance(NumberLines) ;
  	double LineReactance(NumberLines) ;
  	double LineSusceptance(NumberLines) ;
  	double LineRATEA(NumberLines) ;
  	double LineRatio(NumberLines) ;
  	double LineShiftAngle(NumberLines) ;
  	double LineMinAngle(NumberLines) ;
  	double LineMaxAngle(NumberLines) ;
  	double MinPowerFlow(NumberLines) ;
  	double MaxPowerFlow(NumberLines) ;

  // group attributes:
  		:type = "UCBlock" ;
  		:id = "0" ;
  data:

   NetworkBlockClassname = "ACNetworkBlock" ;

   GeneratorNode = 0, 399 ;

   ActivePowerDemand =
  0,
  0.4,
  0.4,
  0.4 ;

   NodeConductance = 0, 0, 0, 0 ;

   NodeSusceptance = 0, 0, 0, 0 ;

   NodeMaxVoltage = 1.1, 1.1, 1.1, 1.1 ;

   NodeMinVoltage = 0.9, 0.9, 0.9, 0.9 ;

   StartLine = 1, 0, 399 ;

   EndLine = 2, 1, 0 ;

   LineResistance = 0.003, 0.003, 0.003 ;

   LineReactance = 0.006, 0.006, 0.006 ;

   LineSusceptance = 0, 0, 0 ;

   LineRATEA = 0, 0, 0 ;

   LineRatio = 0, 0, 1.025 ;

   LineShiftAngle = 0, 0, 0 ;

   LineMinAngle = -360, -360, -360 ;

   LineMaxAngle = 360, 360, 360 ;

   MinPowerFlow = -10000, -10000, -10000 ;

   MaxPowerFlow = 10000, 10000, 10000 ;

  group: UnitBlock_0 {
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 0 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 10 ;

     MinReactivePower = -10 ;

     VoltageMagnitude = 1.05 ;

     MaxPower = 10 ;

     MinPower = 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_0

  group: UnitBlock_1 {
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 0 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 10 ;

     MinReactivePower = -10 ;

     VoltageMagnitude = 1.05 ;

     MaxPower = 10 ;

     MinPower = 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_1
  } // group Block_0
}
