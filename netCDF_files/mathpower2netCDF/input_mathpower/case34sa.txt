netcdf case34sa {

// global attributes:
		:SMS++_file_type = 1 ;
		:baseMVA = 1. ;

group: Block_0 {
  dimensions:
  	NumberElectricalGenerators = 1 ;
  	NumberNodes = 34 ;
  	NumberLines = 33 ;
  	TimeHorizon = 1 ;
  	NumberUnits = 1 ;
  variables:
  	string NetworkBlockClassname ;
  	uint GeneratorNode(NumberElectricalGenerators) ;
  	double ActivePowerDemand(NumberNodes, TimeHorizon) ;
  	double NodeConductance(NumberNodes) ;
  	double NodeSusceptance(NumberNodes) ;
  	double NodeMaxVoltage(NumberNodes) ;
  	double NodeMinVoltage(NumberNodes) ;
  	uint StartLine(NumberLines) ;
  	uint EndLine(NumberLines) ;
  	double LineResistance(NumberLines) ;
  	double LineReactance(NumberLines) ;
  	double LineSusceptance(NumberLines) ;
  	double LineRATEA(NumberLines) ;
  	double LineRatio(NumberLines) ;
  	double LineShiftAngle(NumberLines) ;
  	double LineMinAngle(NumberLines) ;
  	double LineMaxAngle(NumberLines) ;
  	double MinPowerFlow(NumberLines) ;
  	double MaxPowerFlow(NumberLines) ;

  // group attributes:
  		:type = "UCBlock" ;
  		:id = "0" ;
  data:

   NetworkBlockClassname = "ACNetworkBlock" ;

   GeneratorNode = 0 ;

   ActivePowerDemand =
  0,
  0.1425,
  0,
  0.1425,
  0.1425,
  0,
  0,
  0.1425,
  0.1425,
  0,
  0.1425,
  0.084,
  0.045,
  0.045,
  0.045,
  0.0075,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.1425,
  0.085,
  0.048,
  0.048,
  0.048,
  0.0345,
  0.0345,
  0.0345,
  0.0345 ;

   NodeConductance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   NodeSusceptance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   NodeMaxVoltage = 1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 
      1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 
      1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1 ;

   NodeMinVoltage = 1, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 
      0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 
      0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ;

   StartLine = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 12, 13, 14, 5, 16, 17, 
      18, 19, 20, 21, 22, 23, 24, 25, 6, 27, 28, 9, 30, 31, 32 ;

   EndLine = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 
      19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 ;

   LineResistance = 0.117, 0.10725, 0.16445, 0.1495, 0.1495, 0.3144, 0.2096, 
      0.3144, 0.2096, 0.131, 0.1048, 0.1572, 0.2096, 0.1048, 0.0524, 0.1794, 
      0.16445, 0.2079, 0.189, 0.189, 0.262, 0.262, 0.3144, 0.2096, 0.131, 
      0.1048, 0.1572, 0.1572, 0.1572, 0.1572, 0.2096, 0.1572, 0.1048 ;

   LineReactance = 0.048, 0.044, 0.04565, 0.0415, 0.0415, 0.054, 0.036, 
      0.054, 0.036, 0.0225, 0.018, 0.027, 0.036, 0.018, 0.009, 0.0498, 
      0.04565, 0.0473, 0.043, 0.043, 0.045, 0.045, 0.054, 0.036, 0.0225, 
      0.018, 0.027, 0.027, 0.027, 0.027, 0.036, 0.027, 0.018 ;

   LineSusceptance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineRATEA = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineRatio = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineShiftAngle = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineMinAngle = -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360 ;

   LineMaxAngle = 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360 ;

   MinPowerFlow = -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000 ;

   MaxPowerFlow = 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000 ;

  group: UnitBlock_0 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	double StartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 0 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 10 ;

     MinReactivePower = -10 ;

     VoltageMagnitude = 1 ;

     MaxPower = 10 ;

     MinPower = 0 ;

     CostModel = 1 ;

     StartupCost = 0 ;

     PowerCostCoeffs = 0, 20, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_0
  } // group Block_0
}
