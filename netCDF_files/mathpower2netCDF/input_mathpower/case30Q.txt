netcdf case30Q {

// global attributes:
		:SMS++_file_type = 1 ;
		:baseMVA = 100. ;

group: Block_0 {
  dimensions:
  	NumberElectricalGenerators = 6 ;
  	NumberNodes = 30 ;
  	NumberLines = 41 ;
  	TimeHorizon = 1 ;
  	NumberUnits = 6 ;
  variables:
  	string NetworkBlockClassname ;
  	uint GeneratorNode(NumberElectricalGenerators) ;
  	double ActivePowerDemand(NumberNodes, TimeHorizon) ;
  	double NodeConductance(NumberNodes) ;
  	double NodeSusceptance(NumberNodes) ;
  	double NodeMaxVoltage(NumberNodes) ;
  	double NodeMinVoltage(NumberNodes) ;
  	uint StartLine(NumberLines) ;
  	uint EndLine(NumberLines) ;
  	double LineResistance(NumberLines) ;
  	double LineReactance(NumberLines) ;
  	double LineSusceptance(NumberLines) ;
  	double LineRATEA(NumberLines) ;
  	double LineRatio(NumberLines) ;
  	double LineShiftAngle(NumberLines) ;
  	double LineMinAngle(NumberLines) ;
  	double LineMaxAngle(NumberLines) ;
  	double MinPowerFlow(NumberLines) ;
  	double MaxPowerFlow(NumberLines) ;

  // group attributes:
  		:type = "UCBlock" ;
  		:id = "0" ;
  data:

   NetworkBlockClassname = "ACNetworkBlock" ;

   GeneratorNode = 0, 1, 21, 26, 22, 12 ;

   ActivePowerDemand =
  0,
  21.7,
  2.4,
  7.6,
  0,
  0,
  22.8,
  30,
  0,
  5.8,
  0,
  11.2,
  0,
  6.2,
  8.2,
  3.5,
  9,
  3.2,
  9.5,
  2.2,
  17.5,
  0,
  3.2,
  8.7,
  0,
  3.5,
  0,
  0,
  2.4,
  10.6 ;

   NodeConductance = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   NodeSusceptance = 0, 0, 0, 0, 0.19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0.04, 0, 0, 0, 0, 0, 0 ;

   NodeMaxVoltage = 1.05, 1.1, 1.05, 1.05, 1.05, 1.05, 1.05, 1.05, 1.05, 
      1.05, 1.05, 1.05, 1.1, 1.05, 1.05, 1.05, 1.05, 1.05, 1.05, 1.05, 1.05, 
      1.1, 1.1, 1.05, 1.05, 1.05, 1.1, 1.05, 1.05, 1.05 ;

   NodeMinVoltage = 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 
      0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 
      0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95 ;

   StartLine = 0, 0, 1, 2, 1, 1, 3, 4, 5, 5, 5, 5, 8, 8, 3, 11, 11, 11, 11, 
      13, 15, 14, 17, 18, 9, 9, 9, 9, 20, 14, 21, 22, 23, 24, 24, 27, 26, 26, 
      28, 7, 5 ;

   EndLine = 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 8, 9, 10, 9, 11, 12, 13, 14, 15, 
      14, 16, 17, 18, 19, 19, 16, 20, 21, 21, 22, 23, 23, 24, 25, 26, 26, 28, 
      29, 29, 27, 27 ;

   LineResistance = 0.02, 0.05, 0.06, 0.01, 0.05, 0.06, 0.01, 0.05, 0.03, 
      0.01, 0, 0, 0, 0, 0, 0, 0.12, 0.07, 0.09, 0.22, 0.08, 0.11, 0.06, 0.03, 
      0.09, 0.03, 0.03, 0.07, 0.01, 0.1, 0.12, 0.13, 0.19, 0.25, 0.11, 0, 
      0.22, 0.32, 0.24, 0.06, 0.02 ;

   LineReactance = 0.06, 0.19, 0.17, 0.04, 0.2, 0.18, 0.04, 0.12, 0.08, 0.04, 
      0.21, 0.56, 0.21, 0.11, 0.26, 0.14, 0.26, 0.13, 0.2, 0.2, 0.19, 0.22, 
      0.13, 0.07, 0.21, 0.08, 0.07, 0.15, 0.02, 0.2, 0.18, 0.27, 0.33, 0.38, 
      0.21, 0.4, 0.42, 0.6, 0.45, 0.2, 0.06 ;

   LineSusceptance = 0.03, 0.02, 0.02, 0, 0.02, 0.02, 0, 0.01, 0.01, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0.02, 0.01 ;

   LineRATEA = 130, 130, 65, 130, 130, 65, 90, 70, 130, 32, 65, 32, 65, 65, 
      65, 65, 32, 32, 32, 16, 16, 16, 16, 32, 32, 32, 32, 32, 32, 16, 16, 16, 
      16, 16, 16, 65, 16, 16, 16, 32, 32 ;

   LineRatio = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineShiftAngle = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ;

   LineMinAngle = -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
      -360, -360, -360, -360, -360, -360, -360 ;

   LineMaxAngle = 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 360, 
      360 ;

   MinPowerFlow = -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, -10000, 
      -10000, -10000, -10000, -10000, -10000, -10000, -10000 ;

   MaxPowerFlow = 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 
      10000, 10000, 10000 ;

  group: UnitBlock_0 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 23.54 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 150 ;

     MinReactivePower = -20 ;

     VoltageMagnitude = 1 ;

     MaxPower = 80 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.02, 2, 0 ;

     ReactivePowerCostCoeffs = 0.02, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_0

  group: UnitBlock_1 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 60.97 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 60 ;

     MinReactivePower = -20 ;

     VoltageMagnitude = 1 ;

     MaxPower = 80 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.0175, 1.75, 0 ;

     ReactivePowerCostCoeffs = 0.0175, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_1

  group: UnitBlock_2 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 21.59 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 62.5 ;

     MinReactivePower = -15 ;

     VoltageMagnitude = 1 ;

     MaxPower = 50 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.0625, 1, 0 ;

     ReactivePowerCostCoeffs = 0.0625, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_2

  group: UnitBlock_3 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 26.91 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 48.7 ;

     MinReactivePower = -15 ;

     VoltageMagnitude = 1 ;

     MaxPower = 55 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.00834, 3.25, 0 ;

     ReactivePowerCostCoeffs = 0.00834, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_3

  group: UnitBlock_4 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 19.2 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 40 ;

     MinReactivePower = -10 ;

     VoltageMagnitude = 1 ;

     MaxPower = 30 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.025, 3, 0 ;

     ReactivePowerCostCoeffs = 0.025, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_4

  group: UnitBlock_5 {
    dimensions:
    	NumberCostCoeffs = 3 ;
    	NumberReactiveCostCoeffs = 3 ;
    variables:
    	double InitialPower ;
    	double InitialReactivePower ;
    	double MaxReactivePower(TimeHorizon) ;
    	double MinReactivePower(TimeHorizon) ;
    	double VoltageMagnitude(TimeHorizon) ;
    	double MaxPower(TimeHorizon) ;
    	double MinPower(TimeHorizon) ;
    	uint CostModel ;
    	uint ReactiveCostModel ;
    	double StartupCost ;
    	double ReactiveStartupCost ;
    	double PowerCostCoeffs(NumberCostCoeffs) ;
    	double ReactivePowerCostCoeffs(NumberReactiveCostCoeffs) ;
    	double LinearTerm ;
    	double MinUpTime ;
    	double MinDownTime ;
    	double InitUpDownTime ;
    	double InertiaCommitment ;

    // group attributes:
    		:type = "ThermalUnitBlock" ;
    data:

     InitialPower = 37 ;

     InitialReactivePower = 0 ;

     MaxReactivePower = 44.7 ;

     MinReactivePower = -15 ;

     VoltageMagnitude = 1 ;

     MaxPower = 40 ;

     MinPower = 0 ;

     CostModel = 1 ;

     ReactiveCostModel = 1 ;

     StartupCost = 0 ;

     ReactiveStartupCost = 0 ;

     PowerCostCoeffs = 0.025, 3, 0 ;

     ReactivePowerCostCoeffs = 0.025, 0, 0 ;

     LinearTerm = 1 ;

     MinUpTime = 1 ;

     MinDownTime = 1 ;

     InitUpDownTime = 1 ;

     InertiaCommitment = 1 ;
    } // group UnitBlock_5
  } // group Block_0
}
