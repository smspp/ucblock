author : Quentin Jacquet.

To convert the matpower files in <input_folder> into netCDF files, use the command

python converter_mathpower2netCDF.py <input_folder>

To see the transcription of the format matlab to netCDF, see format_patpower2netCDF.py
(You can design which columns of the input file need to be kept in the output file, and with which names).